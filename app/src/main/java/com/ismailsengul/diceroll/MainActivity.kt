package com.ismailsengul.diceroll



import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

/**
 *This activity allows the user to roll a dice and view the result on the screen
 */

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton: Button = findViewById(R.id.button)
        rollButton.setOnClickListener {
            rollDice()
        }
    }
    /**
     * Roll the dice and update the screen with the result
     */
    private fun rollDice(){
        val dice = Dice(6)
        val diceRoll1 = dice.roll()
        val diceRoll2 = dice.roll()

        val diceImage1: ImageView =findViewById(R.id.imageView1)
        val diceImage2: ImageView =findViewById(R.id.imageView2)

        if (diceRoll1==1){
            diceImage1.setImageResource(R.drawable.dice_1)
        }else if (diceRoll1==2){
            diceImage1.setImageResource(R.drawable.dice_2)
        }else if (diceRoll1==3){
            diceImage1.setImageResource(R.drawable.dice_3)
        }else if (diceRoll1==4){
            diceImage1.setImageResource(R.drawable.dice_4)
        }else if (diceRoll1==5){
            diceImage1.setImageResource(R.drawable.dice_5)
        }else if (diceRoll1==6){
            diceImage1.setImageResource(R.drawable.dice_6)
        }

        if (diceRoll2==1){
            diceImage2.setImageResource(R.drawable.dice_1)
        }else if (diceRoll2==2){
            diceImage2.setImageResource(R.drawable.dice_2)
        }else if (diceRoll2==3){
            diceImage2.setImageResource(R.drawable.dice_3)
        }else if (diceRoll2==4){
            diceImage2.setImageResource(R.drawable.dice_4)
        }else if (diceRoll2==5){
            diceImage2.setImageResource(R.drawable.dice_5)
        }else if (diceRoll2==6){
            diceImage2.setImageResource(R.drawable.dice_6)
        }
    }

}

/**
 * Creates the Dice class and roll function
 */

class Dice(private val numSides: Int){
    fun roll(): Int {
        return (1..numSides).random()
    }
}